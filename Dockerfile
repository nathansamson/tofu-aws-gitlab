FROM ruby:3.3-bookworm

RUN apt-get update && \
    apt-get install git jq bash awscli curl procps -y && \
    apt-get clean

RUN curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh && \
	chmod +x install-opentofu.sh && \
	./install-opentofu.sh --install-method deb && \
	rm -rf install-opentofu.sh

RUN curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb" && \
    dpkg -i session-manager-plugin.deb && \
    rm session-manager-plugin.deb

WORKDIR /scripts
COPY scripts/ .

WORKDIR /root
ENV PATH=/scripts:$PATH


